import { Response, Request, Router } from "express";
import { AttributeModel } from '../models/Attribute';
import { ControllerUtils } from "./ControllerUtils";

export class AttributeController {

    public router: Router;
    
    constructor() {
        this.router = Router();
        this.configureRoutes();
    }

    getAll(req: Request, res: Response) {
        ControllerUtils.getAll(req, res, AttributeModel);
    }


 
    private configureRoutes() {
        
        this.router.route('/')
            .get(this.getAll);
            
    }
}