import { Response, Request, Router } from "express";
import { UserBehaviorModel } from '../models/UserBehavior';
import { ControllerUtils } from "./ControllerUtils";

export class UserBehaviorController {

    public router: Router;

    constructor() {
        this.router = Router();
        this.configureRoutes();
    }

    getAll(req: Request, res: Response) {
        ControllerUtils.getAll(req, res, UserBehaviorModel);
    }

    get(req: Request, res: Response) {
        ControllerUtils.get(req, res, UserBehaviorModel);
    }

    post(req: Request, res: Response) {

        const userBehavior = { 
            userActionId: req.body.userActionId,
            userId: req.body.userId,
            surveyId: req.body.surveyId,
            propertyId: req.body.propertyId
        };

        ControllerUtils.post(req, res, userBehavior, UserBehaviorModel);
        
    }

    private configureRoutes() {
        
        this.router.route('/')
            .get(this.getAll)
            .post(this.post);

        this.router.route('/:id')
            .get(this.get);
            
    }

}