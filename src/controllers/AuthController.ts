// bcryptjs
import * as bcrypt from 'bcryptjs';

// jsonwebtoken
import * as jwt from 'jsonwebtoken';

// express
import { Response, Request, NextFunction, Router } from "express";

// local
import { ApiUserModel } from '../models/ApiUser';
import { Config } from '../Config';

const config: Config = require('../../config.json');   

export class AuthController {

    private static defaultExpiry: number = 86400;
    public router: Router;

    constructor() {
        this.router = Router();
        this.configureRoutes();
    }

    private configureRoutes() {
        
        this.router.route('/')
            .post(this.getAccessToken);
    
    }

    static async verifyAccessToken(req: Request, res: Response, next: NextFunction) {

        const token = req.get("token");

        if(!token) { 
            res.status(403).json({ message: 'No access token found' });
            return;
        }

       try { 
           jwt.verify(token as string, config.secret);
       } catch(err) {
            res.status(403).json({ message: 'Failed to authenticate token' });
            return;
       }
       

       next();

    }

    async getAccessToken(req: Request, res: Response) {

        let returnDocument;

        if(!req.body || !req.body.password || !req.body.name) {
            res.status(401).json({ message: 'Invalid credentials' });
            return;
        }

        try {
            returnDocument = await ApiUserModel.find({ name: req.body.name });
        } catch(err) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        if(!returnDocument || returnDocument.length == 0) {
            res.status(401).json({ message: 'Invalid credentials' });
            return;
        }

        const validHashedPassword: string = returnDocument[0].password;        
        const passwordIsValid: boolean = bcrypt.compareSync(req.body.password, validHashedPassword);

        if(!passwordIsValid) {
            res.status(401).json({ message: 'Invalid credentials' });
            return;
        }
        
        const token: string = jwt.sign({ id: returnDocument._id }, config.secret, { expiresIn: AuthController.defaultExpiry });
        res.json({ token: token, expiry: AuthController.defaultExpiry });

    }

}