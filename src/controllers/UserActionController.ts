import { Response, Request, Router } from "express";
import { UserActionModel } from '../models/UserAction';
import { ControllerUtils } from "./ControllerUtils";

export class UserActionController {
    
    public router: Router;

    constructor() {
        this.router = Router();
        this.configureRoutes();
    }

    getAll(req: Request, res: Response) {
        ControllerUtils.getAll(req, res, UserActionModel);
    }

    get(req: Request, res: Response) {
        ControllerUtils.get(req, res, UserActionModel);
    }

    post(req: Request, res: Response) {

        const userAction = { 
            name: req.body.name,
            decay: req.body.decay,
            weight: req.body.weight,
        };

        ControllerUtils.post(req, res, userAction, UserActionModel);
        
    }

    private configureRoutes() {
        
        this.router.route('/')
            .get(this.getAll)
            .post(this.post);

        this.router.route('/:id')
            .get(this.get);
            
    }
    
}