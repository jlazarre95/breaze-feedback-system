import { Request, Response } from "express";
import { Model, Document } from "mongoose";

export class ControllerUtils {

    static async getAll(req: Request, res: Response, model: Model<Document>) {

        let returnDocument;
        
        try {
            returnDocument = await model.find();
        } catch(err) {
            res.status(500).json({ message: 'Could not get objects' });
            return;
        }

        if(!returnDocument) 
            res.status(500).json({ message: 'Could not get objects' });
        else 
            res.json(returnDocument);

    }

    static async get(req: Request, res: Response, model: Model<Document>) {

        let returnDocument;
        
        try {
            returnDocument = await model.findById(req.params['id']);
        } catch(err) {
            res.status(400).json({ message: 'Could not get object ' + req.params['id'] });
            return;
        }

        if(!returnDocument) 
            res.sendStatus(404);
        else 
            res.json(returnDocument);

        
    }

    static async post(req: Request, res: Response, object: Object, model: Model<Document>) {

        let returnDocument: Document;        
        try {
            returnDocument = await model.create(object);
        } catch(err) {
            res.status(400).json({ message: 'Could not post object' });
            return;
        }

        if(!returnDocument) 
            res.status(500).json({ message: 'Could not post object' });
        else
            res.json(returnDocument);

    }

    static async patchSurveyByUser(req: Request, res: Response, object: Object, model: Model<Document>){
        
        let returnDocument;
        
        try {
            returnDocument = await model.findOneAndUpdate({userId : req.params['id']}, {$set: object}, {new: true});
        } catch(err) {
            res.status(400).json({ message: 'Could not update object '});
            return;
        }

        if(!returnDocument) 
            res.sendStatus(404);
        else 
            res.json(returnDocument);
        
    }

    static async getSurveyByUser(req: Request, res: Response, model: Model<Document>) {
        
        let returnDocument;
        
        try {
            returnDocument = await model.findOne({userId : req.params['id']});
        } catch(err) {
            res.status(400).json({ message: 'Could not get survey ' + req.params['id'] });
            return;
        }

        if(!returnDocument) 
            res.sendStatus(404);
        else 
            res.json(returnDocument);

        
    }


}