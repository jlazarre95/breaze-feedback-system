import { Response, Request, Router } from "express";
import { SurveyModel } from '../models/Survey';
import { ControllerUtils } from "./ControllerUtils";

export class SurveyController {

    public router: Router;
    
    constructor() {
        this.router = Router();
        this.configureRoutes();
    }

    getAll(req: Request, res: Response) {
        ControllerUtils.getAll(req, res, SurveyModel);
    }

    get(req: Request, res: Response) {
        ControllerUtils.getSurveyByUser(req, res, SurveyModel);
    }

    post(req: Request, res: Response) {

        const survey = {
            userId: req.body.userId,
            results: req.body.results
        };

        ControllerUtils.post(req, res, survey, SurveyModel);

    }

    patch(req: Request, res: Response){

        const survey = {
            userId: req.body.userId,
            results: req.body.results
        };

        ControllerUtils.patchSurveyByUser(req, res, survey, SurveyModel);
    }

    private configureRoutes() {
        
        this.router.route('/')
            .get(this.getAll)
            .post(this.post);

        this.router.route('/:id')
            .get(this.get)
            .patch(this.patch);
            
    }
}