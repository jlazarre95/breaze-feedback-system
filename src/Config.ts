export interface Config {
    databaseConn: string;
    port: string;
    secret: string;
}