// Express
import * as express from 'express';
import * as bodyParser from 'body-parser';

// Mongo
import { Db, MongoClient } from 'mongodb';

// Mongoose
import mongoose = require('mongoose');
mongoose.Promise = global.Promise;  // Use native ES6 promises.

// Morgan
import * as morgan from 'morgan';

// Swagger
import * as swaggerUi from 'swagger-ui-express';

// Local
import { Config } from './Config';
import { AuthController } from './controllers/AuthController';
import { UserBehaviorController } from './controllers/UserBehaviorController';
import { UserActionController } from './controllers/UserActionController';
import { SurveyController } from './controllers/SurveyController';
import { AttributeController } from './controllers/AttributeController';
import { UserBehaviorModel } from './models/UserBehavior';
import { UserActionModel } from './models/UserAction';
import { AggregationService } from './services/AggregationService';

const appConfig: Config = require('../config.json');
const swaggerDocument = require('../swagger.json');

/**
 * Represents the application server for the feedback system microservice.
 * @author Jahkell Lazarre
 */
class App {

    private app: express.Express;
    private authController: AuthController;
    private userBehaviorController: UserBehaviorController;
    private userActionController: UserActionController;
    private aggregationService: AggregationService;
    private surveyController: SurveyController;
    private attributeController: AttributeController;

    constructor(private config: Config, private aggregationInterval: number) {
        this.app = express();
        this.authController = new AuthController();
        this.userBehaviorController = new UserBehaviorController();
        this.userActionController = new UserActionController();
        this.surveyController = new SurveyController();
        this.attributeController = new AttributeController();
        this.aggregationService = new AggregationService(aggregationInterval);
        this.initMiddleware();
    }

    public async start() {
        console.log('Connecting to database');
        await this.connectToDb();
        this.app.listen(this.config.port, () => { 
            console.log('App listening on port ' + this.config.port);
            this.aggregationService.run();            
        });
    }

    private initMiddleware() {
        
        this.app.use(bodyParser.json());
        this.app.use(morgan('dev'));

        this.app.use((req, res, next) => { 
            // NOTE: Cross-Origin related code should be removed before running in production!
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, token, Authorization");
            //
            // This allows the survey PATCH to operate correctly
            res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PATCH");
            //
            //

            if(req.method == "OPTIONS") res.send(200);
            else next();
        });

        this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

        this.app.use('/api', (req, res, next) => {
            AuthController.verifyAccessToken(req, res, next);
        });

        this.app.use('/authtoken/', this.authController.router);
        this.app.use('/api/user-behavior', this.userBehaviorController.router); 
        this.app.use('/api/user-action', this.userActionController.router);
        this.app.use('/api/survey', this.surveyController.router);
        this.app.use('/api/attribute', this.attributeController.router);
        this.app.get('/', (req, res) => {
            res.setHeader('Content-Type', 'application/json');
        });
    
    }

    private async connectToDb() {
        await mongoose.connect(this.config.databaseConn, { useMongoClient: true });
    }

}

// Start app server.
const app = new App(appConfig, 60000 * 30); // aggregate every 30 mins
app.start();