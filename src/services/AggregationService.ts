import { UserBehaviorModel } from "../models/UserBehavior";
import { UserActionModel } from "../models/UserAction";
import { Document } from "mongoose";
import { SurveyModel } from "../models/Survey";
import { PropertyModel } from "../models/Property";
import { AttributeModel } from "../models/Attribute";

export class AggregationService {

    private running: boolean = false;

    constructor(private interval: number) {
        
    }

    public run() {
        setInterval(async () => { 
            if(this.running == false) {
                this.running = true;
                await this.runAggregation(); 
                this.running = false;
            }
        } , this.interval);
    }

    private async runAggregation() {

        console.log('Running aggregation service');

        // Get all user behaviors.
        let userBehaviors: Document[] = await UserBehaviorModel.find();
        
        // Iterate over user behaviors.
        let numAttributeScoresAffected: number = 0;
        for(let i = 0; i < userBehaviors.length; i++) {

            const userBehavior: any = userBehaviors[i]; 
            
            // Get all objects.
            const survey: any       = await SurveyModel.findById(userBehavior.surveyId);
            const userAction: any   = await UserActionModel.findById(userBehavior.userActionId);
            const property: any     = await PropertyModel.findOne({PropertyId: userBehavior.propertyId as number});          

            if(!survey || !userAction || !property) continue;

            // Get affected attributes.
            const affectedAttributeNames: string[]  = this.getAffectedAttributes(userAction.name);
            const affectedAttributes: any[]         = await (AttributeModel as any).findWithNames(affectedAttributeNames);

            if(affectedAttributes.length == 0) continue;

            // Iterate over property scores.
            for(let j = 0; j < property.scores.length; j++) {

                const attributeScore: any       = property.scores[j];
                const propertyAttribute: any    = await AttributeModel.findById(attributeScore.attributeId);              

                if(!propertyAttribute) continue;

                // Affect property score.
                if(affectedAttributeNames.indexOf(propertyAttribute.name) >= 0) {
                    const surveyResult: ISurveyResult  = this.getSurveyResult(survey, propertyAttribute.name); // or 0 if none;
                    attributeScore.value += surveyResult.ranking * userAction.weight * surveyResult.importance; // weight should be less than 1
                    numAttributeScoresAffected++;                  
                }

            }

            PropertyModel.create(property);         // Update property scores.
            userAction.weight *= userAction.decay;  // Apply decay and update user action weight.
            UserActionModel.create(userAction);

        }

        console.log(`Finished aggregation service. Number of attribute scores affected: ${numAttributeScoresAffected}`);        

    }

    private getAffectedAttributes(userActionName: string): string[] {

        if(userActionName == 'action_clickProperty') {
            return ['attribute_bedroom', 'attribute_bathroom', 'attribute_price', 'attribute_sqrt', 'attribute_built',
                'attribute_type'];
        } else if(userActionName == 'action_favoriteProperty') {
            return ['attribute_school', 'attribute_mall', 'attribute_grocery', 'attribute_restaurant', 'attribute_bank',
                'attribute_healthcare'];
        } else if(userActionName == 'action_shareProperty') { 
            return ['attribute_gym', 'attribute_pool', 'attribute_lake', 'attribute_seaside', 'attribute_park',
                'attribute_quiet'];
        } else {
            throw new Error(`Unrecognized attribute name: ${userActionName}`);            
        }

    }

    private getSurveyResult(survey: any, attributeName: string): ISurveyResult {       

        for(let i = 0; i < survey.results.length; i++) {
            const result = survey.results[i];
            if(result.answer == 'Yes') result.answer = 1;
            if(result.answer == 'No') result.answer = 0;
            if(result.attribute == attributeName)
                return { ranking: result.answer, importance: result.ranking };
        }

        return { ranking: 0, importance: 0 };

    }

}

interface ISurveyResult {
    ranking: number;
    importance: number;
}
