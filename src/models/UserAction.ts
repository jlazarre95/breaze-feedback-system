// Mongoose
import * as mongoose from 'mongoose';

const userActionSchema = new mongoose.Schema({
    name: { type: String, required: true },
    decay: { type: Number, required: true },
    weight: { type: Number, required: true }
}, { collection: 'user_action_model' });

export const UserActionModel = mongoose.model('UserAction', userActionSchema);