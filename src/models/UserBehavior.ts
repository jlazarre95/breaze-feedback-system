// Mongoose
import * as mongoose from 'mongoose';

const userBehaviorSchema = new mongoose.Schema({
    userActionId: { type:  mongoose.Schema.Types.ObjectId, required: true },
    userId: { type: String, required: true },
    surveyId: { type:  mongoose.Schema.Types.ObjectId, required: false },
    propertyId: { type: String, required: true }, 
}, { collection: 'user_behavior_model' });

export const UserBehaviorModel = mongoose.model('UserBehavior', userBehaviorSchema);