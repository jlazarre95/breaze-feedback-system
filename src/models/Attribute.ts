// Mongoose
import * as mongoose from 'mongoose';

const attributeSchema = new mongoose.Schema({
    name: { type: String, required: true },
    show: { type: String, required: true }, 
}, { collection: 'attribute_model' });

attributeSchema.statics.findWithNames = function(names: string[]): Promise<any> {
    return this.find({ name: names});
}

export const AttributeModel = mongoose.model('Attribute', attributeSchema);