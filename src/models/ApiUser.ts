// Mongoose
import * as mongoose from 'mongoose';

const apiUserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    password: { type: String, required: true }
}, { collection: 'api_user_model' });

export const ApiUserModel = mongoose.model('ApiUser', apiUserSchema);