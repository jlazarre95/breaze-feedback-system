// Mongoose
import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

const propertySchema = new mongoose.Schema({
    _id: { attributeId: mongoose.Schema.Types.ObjectId },
    PropertyId: { type: Number, required: false },   
    scores: [{ attributeId: mongoose.Schema.Types.ObjectId, value: Schema.Types.Mixed }] 
}, { collection: 'property_model' });

export const PropertyModel = mongoose.model('Property', propertySchema);