//Mongoose
import * as mongoose from 'mongoose';

const surveySchema = new mongoose.Schema({
    userId: {type: String, required: true},
    results: {type: [], required: true}
}, { collection: 'survey_model' , versionKey: false});


export const SurveyModel = mongoose.model('Survey', surveySchema);
