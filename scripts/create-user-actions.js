db.getCollection('user_action_model').insert([
    {
        name: 'action_clickProperty',
        decay: 0.99,
        weight: 0.01
    },
    {
        name: 'action_favoriteProperty',
        decay: 0.70,
        weight: 0.35
    },
    {
        name: 'action_shareProperty',
        decay: 0.80,
        weight: 0.20
    }
]);