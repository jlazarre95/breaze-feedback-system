db.getCollection('attribute_model').insert([
    /* 1 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655edd"),
        "name" : "attribute_bedroom",
        "show" : "bedroom"
    },

    /* 2 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ede"),
        "name" : "attribute_bathroom",
        "show" : "bathroom"
    },

    /* 3 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655edf"),
        "name" : "attribute_price",
        "show" : "price"
    },

    /* 4 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee0"),
        "name" : "attribute_sqrt",
        "show" : "sqrt"
    },

    /* 5 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee1"),
        "name" : "attribute_built",
        "show" : "built"
    },

    /* 6 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee2"),
        "name" : "attribute_type",
        "show" : "type"
    },

    /* 7 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee3"),
        "name" : "attribute_school",
        "show" : "school"
    },

    /* 8 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee4"),
        "name" : "attribute_mall",
        "show" : "mall"
    },

    /* 9 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee5"),
        "name" : "attribute_grocery",
        "show" : "grocery"
    },

    /* 10 */
    {
        "_id" : ObjectId("5a1cea51bec090235b655ee6"),
        "name" : "attribute_restaurant",
        "show" : "restaurant"
    },

    /* 11 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655ee7"),
        "name" : "attribute_bank",
        "show" : "bank"
    },

    /* 12 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655ee8"),
        "name" : "attribute_healthcare",
        "show" : "healthcare"
    },

    /* 13 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655ee9"),
        "name" : "attribute_gym",
        "show" : "gym"
    },

    /* 14 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655eea"),
        "name" : "attribute_pool",
        "show" : "pool"
    },

    /* 15 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655eeb"),
        "name" : "attribute_lake",
        "show" : "lake"
    },

    /* 16 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655eec"),
        "name" : "attribute_seaside",
        "show" : "seaside"
    },

    /* 17 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655eed"),
        "name" : "attribute_park",
        "show" : "park"
    },

    /* 18 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655eee"),
        "name" : "attribute_quiet",
        "show" : "quiet"
    },

    /* 19 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655eef"),
        "name" : "attribute_beautiful",
        "show" : "beautiful"
    },

    /* 20 */
    {
        "_id" : ObjectId("5a1cea52bec090235b655ef0"),
        "name" : "attribute_clean",
        "show" : "clean"
    }
]);