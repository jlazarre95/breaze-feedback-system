# BreazeHome Feedback System API

- [BreazeHome Feedback System API](#breazehome-feedback-system-api)
    - [Installation](#installation)
    - [Execution](#execution)
    - [Documentation](#documentation)
    - [Authentication](#authentication)

## Installation

__Prerequisites__

You need the latest versions of:

* node
* npm

__Installation__

To clone repo and install dependencies, run:

    git clone https://bitbucket.org/jlazarre95/breaze-feedback-system.git
    cd breaze-feedback-system
    npm install


Install Typescript and transpile code to Javascript in a local `dist` folder.

    npm install -g tsc 
    tsc

If developing, run ```tsc && tsc -w``` instead of `tsc` to first transiple to 
Javascript and then run tsc in watch mode.

__Configuration__

Add a local `config.json` file for configuring the API. The following is an example
configuration:

```json
{
    "databaseConn": "mongodb://<host>:<port>/<db>",
    "port": 7000,
    "secret": "somesecretkey"
}
```

__Configuration Options__:

* __databaseConn__: Connection string to connect to Mongo (required)
* __port__: Port to run API server on (required)
* __secret__: Secret key for data encryption (can be anything) (required)

__Database Setup__:

The database needs to be seeded with some data in order for the feedback system to 
work. It relies on a running MongoDB instance (can be local or remote). To install
MongoDB, you can refer [here](https://docs.mongodb.com/manual/installation/). However, it is pretty simple to install and run on Linux:

```sh
sudo apt-get install mongodb
mongod
```

_If that didn't work, refer to the above link._

To seed the database with data, simply open a client connection to the mongo server 
(preferably through some GUI like 
MongoChef/RoboMongo/3T) and run the `Javascript` files in the `scripts` folder, e.g.,
load or copy and paste the JS files through the text editor in the GUI and run them. 

Although not all the collections need to be seeded to have it working, to avoid any
potential errors, it's best to just generate all the collections using the scripts.

The `property_model` collection is the only one that requires a separate script and
has more complex dependencies. If how to seed the data for this collection is not
documented, please contact Hao Ren at [hren004@fiu.edu]().

## Execution

After transpiling to Javascript, generating a seeded MongoDB instance, and creating
the configuration file, run `npm start`.

__Note__: If using a port below `1024` in the configuration file, you may need to run
`npm start` with administrator privileges (e.g., `sudo`).

## Documentation

For API documentation, you must have deployed the feedback system API
and visited `/api-docs` in your browser, e.g., `http://localhost:7000/api-docs`

## Authentication

Perform a `POST` to `/authtoken` and pass in the login credentials through the body of the request:

```json
{
    "name": "someusername",
    "password": "somepassword"
}
```

If the credentials are valid, then the server response will look like the following
with a status code of `200`:

```json
{
    "token": "sometoken",
    "expiry": 86400
}
```

You can then put the returned access token in the Swagger 
Authorization header hosted on `/api-docs` for executing requests
in Swagger.